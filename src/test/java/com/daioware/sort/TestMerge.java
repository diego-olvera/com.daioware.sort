package com.daioware.sort;

import java.util.Comparator;

import com.daioware.sort.SortArray;

public class TestMerge {
	
	public static void main(String[] args) {
		Comparator<Integer> comp=Integer::compare;
		Integer[] integers=new Integer[10];
		for(int i=integers.length-1;i>=0;i--) {
			integers[i]=i+1;
		}
		SortArray.mergeSort(integers,integers.length, comp);
		for(Integer n:integers) {
			System.out.println("n="+n);
		}
	}
}
