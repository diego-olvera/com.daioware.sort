package com.daioware.sort;


public enum SortAlgorithm{
	BUBBLE_SORT,
	SELECTION_SORT,
	INSERT_SORT,
	SHELL_SORT,
	QUICK_SORT,
	MERGE_SORT
}
