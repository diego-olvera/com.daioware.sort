package com.daioware.sort;

import java.util.Comparator;

public class SortArray{
			 
	public static<T> void sort(SortAlgorithm algorithm,T[]elems, int n,Comparator<T> comparator){
		switch(algorithm){ 
			case BUBBLE_SORT:bubbleSort(elems, n,comparator);break;
			case SELECTION_SORT:selectionSort(elems, n,comparator);break;
			case INSERT_SORT:insertSort(elems, n,comparator);break;
			case SHELL_SORT:shellSort(elems, n,comparator);break;
			case MERGE_SORT:mergeSort(elems, n,comparator);break;
			case QUICK_SORT:initialQuickSort(elems, n,comparator);break;
			default:;	
		}
	}
	public static<T> void sort(T[]elems,Comparator<T> comparator){
		sort(SortAlgorithm.SHELL_SORT,elems,elems.length,comparator);
	}
	public static<T> void sort(SortAlgorithm algorithm,T[]elems,Comparator<T> comparator){
		sort(algorithm,elems,elems.length,comparator);
	}
	public static<T> void swap(T[]elems, int i,int j){
		T temp=elems[j];
    	elems[j]=elems[i];
    	elems[i]=temp;
	}
	public static<T> void bubbleSort(T[] elems,Comparator<T> comparador) {
		bubbleSort(elems,elems.length,comparador);
	}
	public static<T> void bubbleSort(T[] elems,int n,Comparator<T> comp){
		int i,j,k,l;
		boolean anySwaps=true;
	    for(i=0,k=n-1;i<k && anySwaps;i++){
	    	anySwaps=false;
	        for(j=n-1,l=i+1;j>=l;j--){         	
	           if(comp.compare(elems[j], elems[j-1])<0){
	        	   swap(elems, j, j-1);
	        	   anySwaps=true;
	           }
	        }
	    }
	}
	public static<T> void insertSort(T[] elems,Comparator<T> comp) {
		insertSort(elems,elems.length,comp);
	}
	private static<T> void insertSort(T elems[],int n,Comparator<T> comp){
	    int i,j;
	    for(i=1;i<n;i++){
	        j=i;        
	        while(j>0 && (comp.compare(elems[j], elems[j-1])<0)){
	            swap(elems, j, j-1);
	            j--;
	        }
	    }
	}
	public static<T> void selectionSort(T[] elems,Comparator<T> comp) {
		selectionSort(elems,elems.length,comp);
	}
	private static<T> void selectionSort(T elems[],int n,Comparator<T> comp){
	    int i,j,lowerIndex;
	    T lowerElement;
	    for(i=0;i<n-1;i++){
	        lowerIndex=i;
	        lowerElement=elems[i];
	        for(j=i+1;j<n;j++){	        	
	            if (comp.compare(elems[j],lowerElement)<0){
	                lowerElement=elems[j];
	                lowerIndex=j;
	            }
	        }
	        if(i!=lowerIndex)
	        	swap(elems, lowerIndex, i);
	    }
	}
	public static<T> void shellSort(T[] elems,Comparator<T> comp) {
		shellSort(elems,elems.length,comp);
	}
	private static<T> void shellSort(T elems[],int n,Comparator<T> comp){
	    int i,j,increment;
	    T temp;
	    increment=n/2;
	    while(increment>0){
	        for(i=increment;i<n;i++){
	            j=i;
	            temp=elems[i];	 	            
	            while((j>=increment)&&(comp.compare(elems[j-increment], temp)>0)){
	                elems[j]=elems[j-increment];
	                j=j-increment;
	            }
	            elems[j]=temp;
	        }
	        increment/=2;
	    }
	}
	private static<T>void initialQuickSort(T elems[],int n,Comparator<T> comp){
	    quickSort(elems,0,n-1,comp);
	}

	private static<T>void quickSort(T elems[],int i,int j,Comparator<T> comp){
	    int k,posPivote;
	    T pivote;
	    posPivote=findPivot(elems,i,j, comp);
	    if (posPivote>=0){
	        pivote=elems[posPivote];
	        k=particion(elems,i,j,pivote, comp);
	        quickSort(elems,i,k-1, comp);
	        quickSort(elems,k,j,comp);
	    }
	}
	private static<T>int findPivot(T elems[],int i,int j,Comparator<T> comp){
	    T firstElement;
	    int k,pivote=-1;

	    firstElement=elems[i];
	    for(k=i+1;k<=j;k++){	    	
	        if (comp.compare(elems[k], firstElement)>0)
	            return k;
	        if (comp.compare(elems[k],firstElement)<0)
	            return i;
	    }
	    return pivote;
	}

	private static<T>int particion(T elems[], int i, int j, T pivot,Comparator<T> comp) {
		int left, right;
		left = i;
		right = j;
		do {
			swap(elems, left, right);			
			while (comp.compare(elems[left], pivot) < 0) {
				left++;
			}			
			while (comp.compare(elems[right],pivot)>=0) {
				right--;
			}
		} while (left < right);
		return left;
	}
	public static<T> void mergeSort(T[] elems,int n,Comparator<T> comp){
		T[] vector1,vector2;
		int n1,n2,x,y;
		if(n>1){
			if (n%2==0){
	            n1=n2=n/2;
	        }
	        else{
	            n1=n/2;
	            n2=n1+1;
	        }
			vector1=getNewArray(n1);
			vector2=getNewArray(n2);
			for(x=0;x<n1;x++)
	            vector1[x]=elems[x];
	        for(y=0;y<n2;x++,y++)
	            vector2[y]=elems[x];
	        mergeSort(vector1,n1,comp);
	        mergeSort(vector2,n2,comp);
	        fusion(vector1,n1,vector2,n2,elems,comp);
	        vector1=null;
	        vector2=null;
		} 
	}
	private static<T> void fusion(T arr1[],int n1,T arr2[],int n2,T dest[],Comparator<T> comp){
	    int x1=0,x2=0,x3=0;
	    while(x1<n1 && x2<n2){
	        if (comp.compare(arr1[x1], arr2[x2])<0) {
	            dest[x3]=arr1[x1];
	            x1++;
	        }
	        else{
	            dest[x3]=arr2[x2];
	            x2++;
	        }
	        x3++;
	    }
	    while(x1<n1){
	        dest[x3]=arr1[x1];
	        x1++;
	        x3++;
	    }
	    while(x2<n2){
	        dest[x3]=arr2[x2];
	        x2++;
	        x3++;
	    }
	}
	@SuppressWarnings("unchecked")
	private static<T> T[] getNewArray(int size){
		return (T[])new Object[size];
	}
}
