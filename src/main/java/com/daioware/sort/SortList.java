
package com.daioware.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class SortList{

	public static<T> void sort(SortAlgorithm alg,List<T>elements, int n,Comparator<T> comparator){
		switch(alg){ 
			case BUBBLE_SORT:bubbleSort(elements, n,comparator);break;
			case SELECTION_SORT:selectionSort(elements, n,comparator);break;
			case INSERT_SORT:insertSort(elements, n,comparator);break;
			case SHELL_SORT:shellSort(elements, n,comparator);break;
			case QUICK_SORT:quickSort(elements, n,comparator);break;
			case MERGE_SORT:mergeSort(elements, n, comparator);break;
			default:;
		}		
	}
	public static<T> void sort(SortAlgorithm alg,ArrayList<T>elements,Comparator<T> comparator){
		sort(alg,elements,elements.size(),comparator);		
	}
	public static<T> void sort(ArrayList<T>elements,Comparator<T> comparator){
		sort(SortAlgorithm.SHELL_SORT,elements,elements.size(),comparator);
	}
	public static<T> ArrayList<T> merge(List<List<T>> lists,Comparator<T> comp){
		ArrayList<T> merged=new ArrayList<>();
		for(List<T> list:lists) {
			for(T x:list) {
				merged.add(getSortedIndex(merged, x, comp),x);
			}
		}	
		return merged;
	}
	public static<T> ArrayList<T> merge(List<T> a,List<T> b,Comparator<T> comp){
		return merge(new ArrayList<>(Arrays.asList(a,b)),comp);
	}
	private static<T> int getSortedIndex(ArrayList<T> merged, T elem, Comparator<T> comp) {
		int index=0,i=0;
		for(T x:merged) {
			if(comp.compare(elem, x)>0) {
				index=i+1;
			}
			i++;
		}
		return index;
	}
	public static<T> void swap(List<T> elements, int i,int j){
		T temp=elements.get(j);
		elements.set(j, elements.get(i));
		elements.set(i, temp);
	}
	public static<T> void bubbleSort(List<T> elements,int n,Comparator<T> comp){
		int i,j,k,l;
		boolean anySwaps=true;
	    for(i=0,k=n-1;i<k && anySwaps;i++){
	    	anySwaps=false;
	        for(j=n-1,l=i+1;j>=l;j--){ 
	        	if(comp.compare(elements.get(j), elements.get(j-1))<0){
	        		swap(elements, j, j-1);
	        		anySwaps=true;
	        	}
	        }
	    }
	}
	public static<T> void insertSort(List<T> elements,int n,Comparator<T> comp){
	    int i,j;
	    for(i=1;i<n;i++){
	        j=i;
	        while(j>0 && (comp.compare(elements.get(j), elements.get(j-1))<0)){
	            swap(elements, j, j-1);
	            j--;
	        }
	    }
	}
	public static<T> void selectionSort(List<T> elements,int n,Comparator<T> comp){
	    int i,j,lowerIndex;
	    T lowerElement;
	    for(i=0;i<n-1;i++){
	        lowerIndex=i;
	        lowerElement=elements.get(i);
	        for(j=i+1;j<n;j++){        	
	            if (comp.compare(elements.get(j),lowerElement)<0){
	                lowerElement=elements.get(j);
	                lowerIndex=j;
	            }
	        }
	        if(i!=lowerIndex)
	        	swap(elements, lowerIndex, i);
	    }
	}
	public static<T> void shellSort(List<T> elements,int n,Comparator<T> comp){
	    int i,j,increment;
	    T temp;
	    increment=n/2;
	    while(increment>0){
	        for(i=increment;i<n;i++){
	            j=i;
	            temp=elements.get(i);	 	            
	            while((j>=increment)&&(comp.compare(elements.get(j-increment), temp))>0){
	            	elements.set(j, elements.get(j-increment));
	                j=j-increment;
	            }
	            elements.set(j, temp);
	        }
	        increment/=2;
	    }
	}
	public static<T> void quickSort(List<T> elementos,int n,Comparator<T> comparador){
	    hiddenQuickSort(elementos,0,n-1,comparador);
	}

	private static<T>void hiddenQuickSort(List<T> elements,int i,int j,
			Comparator<T> comp){
	    int k,pivotPos;
	    T pivot;
	    pivotPos=findPivot(elements,i,j,comp);
	    if (pivotPos>=0){
	        pivot=elements.get(pivotPos);
	        k=partition(elements,i,j,pivot,comp);
	        hiddenQuickSort(elements,i,k-1,comp);
	        hiddenQuickSort(elements,k,j,comp);
	    }
	}

	private static<T>int findPivot(List<T> elements,int i,int j,Comparator<T> comp){
	    T primeraClave;
	    int k,pivote=-1;
	    primeraClave=elements.get(i);
	    for(k=i+1;k<=j;k++){	    	
	        if (comp.compare(elements.get(j),primeraClave)>0)
	            return k;
	        if (comp.compare(elements.get(k),primeraClave)<0)
	            return i;
	    }
	    return pivote;
	}

	private static<T>int partition(List<T> elements,int i,int j,T pivot,Comparator<T> comp) {
		int izq, der;
		izq = i;
		der = j;
		do {
			swap(elements, izq, der);		
			while (comp.compare(elements.get(izq), pivot) < 0) {
				izq++;
			}
			while (comp.compare(elements.get(der), pivot)>=0) {
				der--;
			}
		} while (izq < der);
		return izq;
	}
	public static<T> void mergeSort(List<T> elements,int n,Comparator<T> comp){
		ArrayList<T> vector1,vector2;
		int n1,n2,x,y;
		if(n>1){
			if (n%2==0){
	            n1=n2=n/2;
	        }
	        else{
	            n1=n/2;
	            n2=n1+1;
	        }
			vector1=new ArrayList<T>();
			vector2=new ArrayList<T>();
			for(x=0;x<n1;x++)
	            vector1.add(x, elements.get(x));
	        for(y=0;y<n2;x++,y++)
	            vector2.add(y,elements.get(x));
	        mergeSort(vector1,n1,comp);
	        mergeSort(vector2,n2,comp);
	        fusion(vector1,n1,vector2,n2,elements,comp);
	        vector1=null;
	        vector2=null;
		} 
	}
	private static<T> void fusion(List<T> arr1,int n1,List<T> arr2,
			int n2,List<T> dest,
			Comparator<T> comp){
	    int x1=0,x2=0,x3=0;

	    while(x1<n1 && x2<n2){
	        if (comp.compare(arr1.get(x1), arr2.get(x2))<0) {
	            dest.set(x3, arr1.get(x1));
	            x1++;
	        }
	        else{
	        	dest.set(x3, arr2.get(x2));
	            x2++;
	        }
	        x3++;
	    }
	    while(x1<n1){
	        dest.set(x3, arr1.get(x1));
	        x1++;
	        x3++;
	    }
	    while(x2<n2){
	    	dest.set(x3, arr2.get(x2));
	        x2++;
	        x3++;
	    }
	}	
}
